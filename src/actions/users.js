export function usersFetchData(url) {
  return async (dispatch) => {
    dispatch({type: 'ITEMS_ISLOADING'});

    await fetch (url)
      .then((response) => {
        if(!response.ok){
          throw Error(response.statusText);
        }
        dispatch({type: 'ITEMS_ISLOADING'});

        return response;
      })
      .then((response) => response.json())
      .then((data) => dispatch({
        type: 'ITEMS_FETCH_DATA_SUCCESS',
        users: data,
       
      }))
      .catch(() => dispatch(usersHasErrored(true)))
  }
}

export function usersHasErrored(bool) {
  return {
    type: 'ITEMS_HAS_ERRORED',
    hasErrored: bool
  }
}

export function usersBack(bool) {
  return {
    type: 'ITEMS_CHANGE_BACK',
    changeBack: bool
  }
}