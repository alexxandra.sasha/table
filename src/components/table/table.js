import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { usersFetchData } from '../../actions/users';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';

const URL = 'https://jsonplaceholder.typicode.com/users'

const Table = () => {
  const { users } = useSelector(state => state.users)
  const [checkedUsers, setChecked] = useState([])
  const [changeBackUsers, setChangeBackUsers] = useState(true);
  const [fieldData, setFieldData] = useState('id');
  const [rowField, setRowField] = useState('');

  const dispatch = useDispatch()

  const setNewData = (field) => {
    const copyData = users.concat();
    let sortData;

    if (changeBackUsers) {
      sortData = copyData.sort(
        (a, b) => {return a[field] > b[field] ? 1 : -1}
      );
    } else {
      sortData = copyData.reverse(
        (a, b) => {return a[field] > b[field] ? 1 : -1}
      );
    }

    console.log(sortData)
    dispatch({
      type: 'ITEMS_FETCH_DATA_SUCCESS',
      users: sortData
    })

    setChangeBackUsers(!changeBackUsers);
  }

  useEffect(() => {
    dispatch(usersFetchData(URL))
  }, [dispatch])


  const checkHandler = (id) => {
    setChecked(checkedUsers.includes(id) 
    ? checkedUsers.filter(it => it !== id) 
    : [...checkedUsers, id])
  }
  
  const checkAll = () => {
    setChecked(
      checkedUsers.length !== users.length 
      ? users.map((it)=> it.id) 
      :[]
    )
  }
/* 
  const headerElement = ['id', 'name', 'email', 'phone', 'choose']
  const renderHeader = () => {
      return users && headerElement.map((key, index) => {
        return <th key={index}>{key.toUpperCase()}</th>
    })
  }
 */
  const renderBody = () => {
    return users && users.map(({ id, name, email, phone }) => {
        return (
            <tr key={id} className={checkedUsers.includes(id) ? 'bg-gray-400': ''} 
            onClick={()=>detailRow(users[id-1])}
            >
                <td className="px-4 py-2" >{id}</td>
                <td className="px-4 py-2">{name}</td>
                <td className="px-4 py-2">{email}</td>
                <td className="px-4 py-2">{phone}</td>
                <td className='check-box'>
                  <input className="mr-2 leading-tight" 
                    type="checkbox" 
                    onChange={()=>checkHandler(id)} 
                    checked={checkedUsers.includes(id)} />
                </td>
            </tr>
        )
    })
  }


if (users.hasErrored) {
  return <p>Sorry! There was an error loading the items</p>;
}

if (users.isLoading) {
    return <p>Loading…</p>;
}  


const Arrow = () => {
  return (
    changeBackUsers? <ArrowDownwardIcon/> : <ArrowUpward/> 
)}

const fieldArrow = (field) => {
  return (
    setNewData(field),
    setFieldData(field)
  )
}

const detailRow = (row) => {
  setRowField(row)
}


return (
  
<>
<div className="w-2/3 rounded overflow-hidden  shadow-lg text-center mt-5">
        
        <h1 id='title'>Users Table</h1>

        
        <div className="md:flex bg-gray-200 md:items-center mb-6">
          <label className="md:w-2/3 block text-gray-500 font-bold">
            <input className="mr-2 leading-tight" 
              type="checkbox" 
              onChange={checkAll} 
              checked={users.length === checkedUsers.length}/>
            <span className="text-sm">
              Choose all!
            </span>
          </label>
        </div>
        <table id='users' className="table-auto text-center content-around">
            <thead>
                {/* <tr>{renderHeader()}</tr> */}
                <tr>
                  <th className="table-item" 
                  onClick={()=>fieldArrow('id')}
                  >Id {fieldData === 'id' ? <Arrow/> : null}
                   </th>
                  <th className="table-item"  onClick={()=>fieldArrow('name')}>Name  {fieldData === 'name' ? <Arrow/> : null}</th>
                  <th className="table-item"  onClick={()=>fieldArrow('email')}>Email {fieldData === 'email' ? <Arrow/> : null} </th>
                  <th className="table-item"  onClick={()=>fieldArrow('phone')}> Phone  {fieldData === 'phone' ? <Arrow/> : null}</th>
                  <th className="table-item" > Checked </th>
                </tr>
            </thead>

            <tbody>
                {renderBody()}
            </tbody>
          
            <tfoot>
              <tr>
      
              <td colSpan={5}>
                  <textarea className="w-full" placeholder={
                    users.filter((it)=> 
                    checkedUsers.includes(it.id))
                    .map(e => e.name ).join(", ")
                  } />
                </td>
               
              </tr>
              <tr>
              <td colSpan={5}>
                 <div>
                  id: {rowField.id}
                  </div>
                  <div>
                  name: {rowField.name}
                  </div>
                  <div>
            
                  email: {rowField.email}
                  </div> 


                </td>
              </tr>

            </tfoot>
          
        </table>

      </div>
</>)


}

export default Table