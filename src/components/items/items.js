import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import { usersFetchData } from '../../actions/users';


const URL = 'https://jsonplaceholder.typicode.com/users'

const Items = () => {
  const [checked, setChecked] = useState([]);
  const {users} = useSelector(state => state.users);
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(usersFetchData(URL))
  }, [dispatch])


  const checkHandler = (id) => {
    setChecked(checked.includes(id) 
    ? checked.filter(it => it !== id) 
    : [...checked, id])
  }

  const renderBody = () => {
    return users.map(({id, name, email, phone}) =>{
      return (
        <tr key={id}>
        <td className="table-item">{id}</td>
        <td className="table-item">{name}</td>
        <td className="table-item">{email}</td>
        <td className="table-item">{phone}</td>
        <td className="table-item"> <input type='checkbox'
           onChange={()=>checkHandler(id)} 
        
        /></td>
    </tr>
      )
    })
  }



  if (users.hasErrored) {
    return <span>Sorry, we remaking the data</span>
  }

  if (users.isLoading) {
    return <span> Loading </span>;
  }




  return (
    <div >
      <h2>Users Table</h2>
      <div>
        Select all 
        <input type='checkbox' />
      </div>
      <table>
        <thead  className='table'>
          <tr>
            <th className="table-item">Id</th>
            <th className="table-item">Name</th>
            <th className="table-item">Email</th>
            <th className="table-item">Phone</th>
            <th className="table-item"> Checked</th>
          </tr>
        </thead>
        <tbody>
          {renderBody()}
        </tbody>
      </table>
      <textarea placeholder={
        users.filter((u)=> 
          checked.includes(u.id)
        )
        .map((u)=> u.name)
        .join(', ')
      } />
    </div>
  )
}


export default Items