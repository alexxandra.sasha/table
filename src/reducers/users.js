const initialState = {
  users: [],
  hasErrored: false,
  isLoading: false,
  changeBack: true
}

export function users(state = initialState, action) {
  switch (action.type) {
    case 'ITEMS_FETCH_DATA_SUCCESS':
      return {
        ...state,
        users: action.users,
        changeBack: !action.changeBack
      }
    case 'ITEMS_ISLOADING':
      return {
        ...state,
        isLoading: !state.isLoading
      }
    
    case 'ITEMS_HAS_ERRORED':
      return {
        ...state,
        hasErrored: action.hasErrored
      }
    case 'ITEMS_CHANGE_BACK':
      return {
        ...state,
        changeBack: action.changeBack
      }
    default: 
      return state;
  }
}